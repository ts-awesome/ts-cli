export enum HttpMethod {
  GET = 'Get',
  POST = 'Post',
  PUT = 'Put',
  PATCH = 'Patch',
  DELETE = 'Delete'
}

const defaultResponse = {
  200: {description: 'OK'},
  400: {description: 'Bad Request'},
  401: {description: 'Unauthorized'},
  403: {description: 'Forbidden'},
};

export const Responses = {
  GET: {
    ...defaultResponse,
    404: {description: 'Not Found'}
  },
  POST: {
    ...defaultResponse,
    201: {description: 'Created'}
  },
  PUT: {
    ...defaultResponse,
    201: {description: 'Created'},
    204: {description: 'No Content'},
    404: {description: 'Not Found'}
  },
  PATCH: {
    ...defaultResponse,
    204: {description: 'No Content'},
    404: {description: 'Not Found'}
  },
  DELETE: {
    ...defaultResponse,
    204: {description: 'No Content'},
    404: {description: 'Not Found'}
  }
};

export enum AttributeType {
  string = 'string',
  number = 'number',
  boolean = 'boolean',
  Date = 'Date'
}

export interface Attribute {
  name: string;
  type: AttributeType;
}

export interface RouteOptions {
  method: string;
  path: string;
  routeName: string;
  requestParam: Attribute[],
  queryParam: Attribute[],
  requestBody: Attribute[],
  description?: string;
}
