import { HttpMethod, RouteOptions } from './interfaces';

export function validateRouteOptions(options: RouteOptions) {
  const {method, path, routeName} = options;
  checkOptionPresence(method);
  if (!(method in HttpMethod)) {
    throw new Error(`unknown method ${method}`);
  }
  checkOptionPresence(path);
  checkOptionPresence(routeName);
}

export function checkOptionPresence(option: string) {
  if (!option) {
    throw new Error(`required option "${option}" is missing`);
  }
}
