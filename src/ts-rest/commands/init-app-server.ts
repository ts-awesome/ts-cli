import { resolve } from 'path';
import { ICmdRuntime } from '../../cmd-runtime';
import { createFolder, generateFile, getSrcPath, getTemplatePath } from '../../utils';

export async function initAppServer(rt: ICmdRuntime) {
  const middlewareFolder = resolve(getSrcPath(), 'middleware');
  await createFolder('middleware', middlewareFolder);
  await generateFile(getTemplatePath('error-handler.ts'), resolve(middlewareFolder, 'error-handler.middleware.ts'), {});
  await generateFile(getTemplatePath('index-middleware.ts'), resolve(middlewareFolder, 'index.ts'), {});
  await generateFile(getTemplatePath('app-server.ts'), resolve(getSrcPath(), 'app-server.ts'), {});
  await generateFile(getTemplatePath('main.ts'), resolve(getSrcPath(), 'main.ts'), {});
}