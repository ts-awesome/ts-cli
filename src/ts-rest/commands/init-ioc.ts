import * as path from 'path';
import { ICmdRuntime } from '../../cmd-runtime';
import { createFolder, generateFile, getTemplatePath } from '../../utils';
import { existsSync } from 'fs';

export async function initIoc(rt: ICmdRuntime) {
  await createFolder('ioc-container', getIocContainerFolderPath());
  await generateFile(getTemplatePath('root-container.ts'), getRootContainerPath(), {});
  await generateFile(getTemplatePath('request-container.ts'), getRequestContainerPath(), {});
  await generateFile(getTemplatePath('index-ioc-container.ts'), getIoCContainerIndexPath(), {});
}

function getIocContainerFolderPath() {
  return path.resolve(process.cwd(), 'src/ioc-container');
}

function getRootContainerPath() {
  return path.resolve(getIocContainerFolderPath(), 'set-up-root-container.ts')
}

function getRequestContainerPath() {
  return path.resolve(getIocContainerFolderPath(), 'set-up-request-container.ts')
}

function getIoCContainerIndexPath() {
  return path.resolve(getIocContainerFolderPath(), 'index.ts')
}

function ensureFolder(path: string) {
  if (!existsSync(path)) {
    throw new Error(`${path} not found. Have you tried running init:ioc`)
  }
}