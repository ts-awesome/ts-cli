import * as fs from 'fs';
import {
  createFolder,
  generateFile,
  getIndexPath,
  getRootRoutePath,
  getRoutePath,
  getRoutesPath, modifyIndexFile,
  objectToString
} from '../../utils';
import { ICmdRuntime } from '../../cmd-runtime';
import { Command } from 'commander';
import { HttpMethod, Responses, RouteOptions } from '../interfaces';
import { validateRouteOptions } from '../validation';

export async function generateRoute(rt: ICmdRuntime, cmd: Command) {
  const options = cmd.opts() as RouteOptions;
  validateRouteOptions(options);

  const data = parseRouteOptions(options);
  const routeName = parseRouteName(data.routeName);

  await ensureRouteFolders(routeName);
  await ensureRouteIndexFiles(routeName);
  await generateFile('route.ts', getRoutePath(data.method, routeName), data);
  await modifyIndexFile(
    getIndexPath(getRoutesPath(routeName)),
    `${data.method.toLowerCase() + '-' + routeName}`);
}

async function ensureRouteFolders(modelName: string) {
  const defaultFolder = getRootRoutePath();
  const routeFolder = getRoutesPath(modelName);
  if (!fs.existsSync(defaultFolder)) {
    throw new Error(`${defaultFolder} not found. Have you tried running init:routes?`);
  } else if (!fs.existsSync(routeFolder)) {
    await createFolder(modelName, routeFolder);
    await fs.promises.appendFile(getIndexPath(defaultFolder), `export * from './${modelName}';\n`);
    await fs.promises.writeFile(getIndexPath(routeFolder), '');
  }
}

async function ensureRouteIndexFiles(modelName: string) {
  const defaultIndex = getIndexPath(getRootRoutePath());
  const routeIndex = getIndexPath(getRoutesPath(modelName));
  if (!fs.existsSync(defaultIndex)) {
    await fs.promises.writeFile(defaultIndex, '');
  } else if (!fs.existsSync(routeIndex)) {
    await fs.promises.writeFile(routeIndex, '');
  }
}

function transformParams(obj: Object, res: string[]) {
  Object.entries(obj).forEach(([key, value]) => {
    if (value) {
      value.forEach(param => {
        res.push(`\n@${key}('${param.name}') ${param.name}: ${param.type}\n`);
      });
    }
  });
  return res.join();
}

function parseRouteOptions(options: RouteOptions) {
  const params = {};
  const openApi = {
    path: generateOpenApiPath(options.path),
    responses: Responses[options.method]
  };
  const result = {
    method: HttpMethod[options.method],
    path: options.path,
    routeName: options.routeName,
    imports: {}
  };
  if (options.requestParam) {
    params['requestParam'] = options.requestParam
  }
  if (options.requestBody) {
    params['requestBody'] = options.requestBody
  }
  if (options.queryParam) {
    params['queryParam'] = options.queryParam
  }
  if (params) {
    result['params'] = transformParams(params, []);
    result.imports['paramImports'] = Object.keys(params).concat('');
  }
  if (options.description) {
    openApi['description'] = options.description;
  }
  result['openApi'] = objectToString(openApi, []);

  return result;
}

function generateOpenApiPath(path: string) {
  const params = path.match(/(?<=:)(\w+)/g);
  let result = path;
  if (params) {
    params.forEach(param => result = result.replace(`:${param}`, `{${param}}`));
  }
  return result;
}

function parseRouteName(name: string): string {
  const result = name
    .replace(/([A-Z])/g, " $1")
    .trim()
    .toLowerCase()
    .split(' ');

  if (result.length === 1) {
    throw new Error('Wrong route name. It should consist with MethodName + ModelName');
  } else {
    return result[1];
  }
}