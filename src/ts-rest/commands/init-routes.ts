import * as path from 'path';
import { ICmdRuntime } from '../../cmd-runtime';
import { createFolder, generateFile, getIndexPath, getSrcPath } from '../../utils';

export async function initRoutes(rt: ICmdRuntime) {
  const routesFolder = path.resolve(getSrcPath(), 'routes');
  const defaultsFolder = path.resolve(routesFolder, 'defaults');

  await createFolder('routes/defaults', defaultsFolder);
  await generateFile('index-routes.ts', getIndexPath(routesFolder), {});
}
