import { Command } from 'commander';
import { ICmdRuntime } from '../cmd-runtime';
import { initIoc } from './commands/init-ioc';
import { existsSync } from 'fs';
import { createFolder, getSrcPath } from '../utils';
import { initAppServer } from './commands/init-app-server';
import { initRoutes } from './commands/init-routes';
import { parseOption } from '../utils/option';
import { generateRoute } from './commands/generate-route';

export async function tsRestCmd(rt: ICmdRuntime) {
  await ensureSrcFolder();
  const program = new Command('ts-rest');
  program.storeOptionsAsProperties(false);

  program
    .command('init')
    .description('Initialize ts-rest server')
    .action(async () => {
      await initIoc(rt);
      await initRoutes(rt);
      await initAppServer(rt);
    });
  program
    .command('init:ioc')
    .description('Init root and request container files')
    .action(() => initIoc(rt));
  program
    .command('init:app-server')
    .description('Init app-server and main files')
    .action(() => initAppServer(rt));
  program
    .command('init:routes')
    .description('Init routes')
    .action(() => initRoutes(rt));

  program
    .command('generate:route')
    .option('--method <method>', 'http method', parseMethod)
    .option('--path <path>')
    .option('--routeName <routeName>')
    .option('--requestParam <requestParam>', 'requestParam', parseOption)
    .option('--queryParam <queryParam>', 'queryParam', parseOption)
    .option('--requestBody <requestBody>', 'requestBody', parseOption)
    .option('--description <description>')
    .action((cmd) => generateRoute(rt, cmd));

  return program;
}

async function ensureSrcFolder() {
  const path = getSrcPath();
  if (!existsSync(path)) {
    await createFolder('src', path);
  }
}

function parseMethod(method: string) {
  return method.toUpperCase();
}