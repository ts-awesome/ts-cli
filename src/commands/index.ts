export * from './core-dependencies';
export * from './eslint';
export * from './eslint-config';
export * from './ts-config';
export * from './ts-scripts';
export * from './folder-structure';
export * from './jest-config';
