import { ICmdRuntime } from '../cmd-runtime';
import * as fs from 'fs';
import * as _ from 'lodash';

const config = {
  rules: {
    semi: [2, 'always'],
    'no-unused-vars': 'off',
    '@typescript-eslint/no-unused-vars': 'error'
  }
};

const fileName = '.eslintrc.json';

export async function eslintConfig (rt: ICmdRuntime, ...args: string[]): Promise<void> {
  let data = config;
  if (fs.existsSync(fileName)) {
    const file = await fs.promises.readFile(fileName, { encoding: 'utf-8' });
    data = JSON.parse(file);
    _.merge(data, config);
  }
  try {
    await fs.promises.writeFile(fileName, JSON.stringify(data, null, '  '));
    rt.logger.log('Eslint config added successfully');
  } catch (err) {
    rt.logger.error(err);
  }
}
