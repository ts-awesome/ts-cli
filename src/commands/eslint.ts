import * as fs from 'fs';
import { ICmdRuntime } from '../cmd-runtime';
import { eslintConfig } from './eslint-config';
import { packageJson } from './package-json';

const standardxConfig = {
  parser: '@typescript-eslint/parser',
  plugins: [
    '@typescript-eslint/eslint-plugin'
  ]
};

const packageJsonFileName = 'package.json';

export async function eslint (rt: ICmdRuntime, ...args: string[]) {
  await eslintConfig(rt, ...args);
  if (!fs.existsSync(packageJsonFileName)) {
    await packageJson(rt, ...args);
  }
  const file = await fs.promises.readFile(packageJsonFileName, { encoding: 'utf-8' });
  const data = JSON.parse(file);
  data.standardx = standardxConfig;

  try {
    await fs.promises.writeFile(packageJsonFileName, JSON.stringify(data, null, '  '));
    rt.logger.log('Standardx rules added successfully');
  } catch (err) {
    rt.logger.error(err);
  }
}
