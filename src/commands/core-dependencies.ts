import * as fs from 'fs';
import { exec } from 'child_process';
import { ICmdRuntime } from '../cmd-runtime';
import { packageJson } from './package-json';

const fileName = 'package.json';
const command = 'npm i -D typescript standardx rimraf nodemon eslint concurrently @types/node @typescript-eslint/eslint-plugin @typescript-eslint/parser';

export async function coreDependencies (rt: ICmdRuntime, ...args: string[]) {
  if (!fs.existsSync(fileName)) {
    await packageJson(rt, ...args);
  }
  try {
    await new Promise((resolve, reject) => {
      exec(command, (error, stdout) => {
        if (error) {
          reject(error);
        }
        resolve(stdout);
      });
    });
    rt.logger.log('Dependencies added successfully');
  } catch (err) {
    rt.logger.error(err);
  }
}
