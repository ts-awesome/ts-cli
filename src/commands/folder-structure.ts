import * as fs from 'fs';
import { ICmdRuntime } from '../cmd-runtime';

const srcFolderName = 'src';
const testsFolderName = 'spec';

export async function folderStructure (rt: ICmdRuntime, ...args: string[]) {
  if (!fs.existsSync(srcFolderName)) {
    await fs.promises.mkdir(srcFolderName);
    rt.logger.log('Src directory created successfully');
  }
  if (!fs.existsSync(testsFolderName)) {
    await fs.promises.mkdir(testsFolderName);
    rt.logger.log('Spec directory created successfully');
  }
}
