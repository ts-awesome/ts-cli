import * as fs from 'fs';
import { ICmdRuntime } from '../cmd-runtime';

const config = `module.exports = {
    testMatch: [
        "**/*.spec.ts"
    ],
    transform: {
        "^.+\\\\.(ts|tsx)$": "ts-jest"
    },
};`;

const fileName = 'jest.config.js';

export async function jestConfig (rt: ICmdRuntime, ...args: string[]) {
  try {
    await fs.promises.writeFile(fileName, config);
    rt.logger.log('Jest config added successfully');
  } catch (err) {
    rt.logger.error(err);
  }
}
