import * as fs from 'fs';
import { ICmdRuntime } from '../cmd-runtime';
import { packageJson } from './package-json';

const scripts = {
  clean: 'echo "Clean..." && rimraf ./dist',
  build: 'echo "Building..." && npm run clean && tsc --project tsconfig-build.json',
  watch: 'echo "Watch..." && tsc -w --project tsconfig-build.json',
  dev: 'echo "Nodemon..." && nodemon --inspect=0.0.0.0:9229 -w ./dist ./dist/main.js',
  test: 'echo "Testing..." && jest',
  'start:development': 'echo "Developing..." "npm run build" && concurrently "npm run watch" "npm run dev"',
  'start:production': 'echo "Starting..." && node index.js',
  standardx: 'standardx **/*.ts',
  'standardx-fix': 'standardx --fix **/*.ts'
};

const fileName = 'package.json';

export async function tsScripts (rt: ICmdRuntime, ...args: string[]) {
  if (!fs.existsSync(fileName)) {
    await packageJson(rt, ...args);
  }
  const file = await fs.promises.readFile(fileName, { encoding: 'utf-8' });
  const data = JSON.parse(file);
  data.scripts = { ...data.scripts, ...scripts };

  try {
    await fs.promises.writeFile(fileName, JSON.stringify(data, null, '  '));
    rt.logger.log('Scripts added successfully');
  } catch (err) {
    rt.logger.error(err);
  }
}
