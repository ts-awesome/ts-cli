import * as fs from 'fs';
import * as _ from 'lodash';
import { ICmdRuntime } from '../cmd-runtime';

const config = {
  compileOnSave: true,
  compilerOptions: {
    target: 'es5',
    module: 'commonjs',
    allowJs: true,
    sourceMap: true,
    outDir: './dist',
    downlevelIteration: true,
    esModuleInterop: true,
    experimentalDecorators: true,
    emitDecoratorMetadata: true,
    lib: [
      'es2015',
      'es2016',
      'es2017',
      'es2018',
      'es2019',
      'es2020'
    ]
  },
  exclude: [
    'dist',
    'node_modules'
  ]
};

const buildConfig = {
  extends: './tsconfig.json',
  exclude: [
    'jest.config.js',
    'spec',
    'test',
    'tests'
  ]
};

const fileName = 'tsconfig.json';
const buildFileName = 'tsconfig-build.json';

export async function tsConfig (rt: ICmdRuntime, ...args: string[]): Promise<void> {
  let data: any = config;
  if (fs.existsSync(fileName)) {
    const file = await fs.promises.readFile(fileName, { encoding: 'utf-8' });
    data = JSON.parse(file);
    _.merge(data, config);
  }
  try {
    await fs.promises.writeFile(fileName, JSON.stringify(data, null, '  '));
    await fs.promises.writeFile(buildFileName, JSON.stringify(buildConfig, null, '  '));
    rt.logger.log('Tsconfig added successfully');
  } catch (err) {
    rt.logger.error(err);
  }
}
