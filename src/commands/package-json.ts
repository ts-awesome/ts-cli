import { ICmdRuntime } from '../cmd-runtime';
import { exec } from 'child_process';

const command = 'npm init -y';

export async function packageJson (rt: ICmdRuntime, ...args: string[]) {
  try {
    await new Promise((resolve, reject) => {
      exec(command, (error, stdout) => {
        if (error) {
          reject(error);
        }
        resolve(stdout);
      });
    });
    rt.logger.log('package.json initialized successfully');
  } catch (err) {
    rt.logger.error(err);
  }
}
