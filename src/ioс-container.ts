import 'reflect-metadata';

import { Container } from 'inversify';
import { getDriver, ILoggerFactory, Logger, LoggerFactorySymbol } from '@viatsyshyn/ts-logger';

export function setupContainer (): Container {
  const container = new Container();

  // BIND LOGGER
  container.bind<ILoggerFactory>(LoggerFactorySymbol)
    .toDynamicValue(() => {
      const driver = getDriver({ type: 'CONSOLE', logLevel: 'debug' });
      const reporter = () => void 0;
      return (name: any) => new Logger(typeof name === 'function' ? name.name : name, driver, reporter);
    }).inSingletonScope();

  return container;
}
