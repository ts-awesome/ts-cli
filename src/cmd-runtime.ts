import 'reflect-metadata';

import { Command } from 'commander';
import { Container } from 'inversify';
import { ILogger, ILoggerFactory, LoggerFactorySymbol } from '@viatsyshyn/ts-logger';
import { coreDependencies, eslint, folderStructure, tsConfig, tsScripts, jestConfig } from './commands';
import { tsRestCmd } from './ts-rest/ts-rest';

const program = new Command();

export interface ICmdRuntime {
  logger: ILogger;

  execute(cmd: string[]): Promise<void>;
}

export class CmdRuntime implements ICmdRuntime {
  public readonly logger: ILogger;

  constructor (container: Container) {
    this.logger = container.get<ILoggerFactory>(LoggerFactorySymbol)(CmdRuntime);
  }

  public async execute (cmd: string[]): Promise<void> {
    const init = program.command('init');

    init
      .command('ts-minimal')
      .action(async ({ args }) => {
        await folderStructure(this, args);
        await tsConfig(this, args);
        await tsScripts(this, args);
        await eslint(this, args);
        await jestConfig(this, args);
        await coreDependencies(this, args);
      });

    init
      .command('ts-config')
      .action(({ args }) => tsConfig(this, args));

    init
      .command('ts-scripts')
      .action(({ args }) => tsScripts(this, args));

    init
      .command('eslint')
      .action(({ args }) => eslint(this, args));

    init
      .command('jest-config')
      .action(({ args }) => jestConfig(this, args));

    init
      .command('core-dependencies')
      .action(({ args }) => coreDependencies(this, args));

    init
      .command('folder-structure')
      .action(({ args }) => folderStructure(this, args));

    program.addCommand(await tsRestCmd(this));

    try {
      await program.parseAsync(cmd);
    } catch (e) {
      this.logger.error(e);
    }
  }
}
