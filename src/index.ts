#!/usr/bin/env node

import 'reflect-metadata';

import { CmdRuntime } from './cmd-runtime';
import { setupContainer } from './ioс-container';

const container = setupContainer();
const cmdRuntime = new CmdRuntime(container);
cmdRuntime.execute(process.argv);

export default cmdRuntime;
