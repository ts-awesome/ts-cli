import * as fs from "fs";
import beautify from 'js-beautify';
import { renderString } from 'template-file';
import { getTemplatePath } from './path';

export async function useTemplate(path: string, data: any) {
  const options = {
    beautify: true,
    indent_size: 2,
    end_with_newline: true,
    preserve_newlines: true
  };

  const template = (await fs.promises.readFile(getTemplatePath(path))).toString();
  const content = renderString(template, data);
  return beautify(content, options);
}

// TODO add the ability to parse without quotes where not needed
export function objectToString(obj: Object, res: string[]) {
  res.push('{');
  Object.entries(obj).forEach(([key, value], index) => {
    res.push(`${key}: `);
    if (typeof value === 'object') {
      objectToString(value, res);
      if (index < Object.keys(obj).length - 1) res.push(',');
    } else {
      res.push(`'${value}'`);
      if (index < Object.keys(obj).length - 1) res.push(',');
    }
  });
  res.push('}');
  return res.join('');
}