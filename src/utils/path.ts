import * as path from "path";

export function getTemplatePath(templatePath: string) {
  return path.resolve(__dirname, '../../', 'assets', templatePath);
}

export function getSrcPath() {
  return path.resolve(process.cwd(), 'src');
}

export function getRootRoutePath(): string {
  return path.resolve(getSrcPath(), 'routes/defaults');
}

export function getRoutesPath(modelName: string): string {
  return path.resolve(getRootRoutePath(), modelName);
}

export function getRoutePath(method: string, routeName: string): string {
  return path.resolve(getRoutesPath(routeName), `${method.toLowerCase()}-${routeName}.ts`)
}

export function getIndexPath(indexFolderPath: string) {
  return path.resolve(indexFolderPath, 'index.ts');
}