export function parseOption(option: string) {
  const properties = option.replace(/\s/g, '').split(',');
  const result = [];

  properties.forEach(property => {
    const parsedProperty = property.split(':');
    if (parsedProperty.length === 2) {
      result.push({name: parsedProperty[0], type: parsedProperty[1]})
    }
  });

  return result;
}