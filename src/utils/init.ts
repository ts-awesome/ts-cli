import * as fs from 'fs';
import { useTemplate } from './assets';
import { existsSync } from "fs";

// TODO: ensure that only 'models/db' folder creates
export async function createFolder(folderName: string, folderPath: string) {
  try {
    if (!fs.existsSync(folderPath)) {
      await fs.promises.mkdir(folderPath, {recursive: true});
      console.log(`Successfully created ${folderName} folder at ${folderPath}`);
    } else {
      console.log(`${folderName} folder at ${folderPath} already exists`);
    }
  } catch (err) {
    console.error(err);
  }
}

export async function generateFile(templatePath: string, distPath: string, data: any) {
  const content = await useTemplate(templatePath, data);
  if (existsSync(distPath)) {
    throw new Error(`File ${distPath} already exists`);
  }
  await fs.promises.writeFile(distPath, content);
}

export async function modifyIndexFile(indexFilePath: string, exportFileName: string) {
  if (!existsSync(indexFilePath)) {
    throw new Error(`File ${indexFilePath} doesn't exists`);
  }
  await fs.promises.appendFile(indexFilePath, `export * from './${exportFileName}'`);
}

