# @viatsyshyn/ts-cli

Tool for generating basic typescript project configurations

# Installation

`npm install -g @viatsyshyn/ts-cli`

# Usage

```bash
@viatsyshyn/ts-cli <command>

Commands:
    @viatsyshyn/ts-cli init ts-minimal          Generates basic configuration for typescript project
    @viatsyshyn/ts-cli init ts-config           Initializes tsconfig
    @viatsyshyn/ts-cli init ts-scripts          Add basic scripts to package.json
    @viatsyshyn/ts-cli init eslint              Initializes code style rules
    @viatsyshyn/ts-cli init jest-config         Initializes test configuration
    @viatsyshyn/ts-cli init core-dependencies   Install core dependencies
    @viatsyshyn/ts-cli init folder-structure    Initialize basic folder structure

Options:
    -h, --help  display help for command
```
