import { Container } from 'inversify';
import { Sanitizer, SanitizerSymbol } from '@viatsyshyn/ts-rest';
import { PoolConfig, Pool } from 'pg';
import { getDriver, ILoggerFactory, Logger, LoggerFactorySymbol } from '@viatsyshyn/ts-logger';
import config from 'config';

export function setUpRootContainer(): Container {

  const container = new Container();

  // BIND LOGGER
  container.bind<ILoggerFactory>(LoggerFactorySymbol)
    .toDynamicValue(() => {
      const driver = getDriver({type: 'CONSOLE', logLevel: 'debug'});
      const reporter = () => void 0;
      return (name: any) => new Logger(typeof name === 'function' ? name.name : name, driver, reporter);
    }).inSingletonScope();

  // BIND DB
  const sqlConfig = config.get<PoolConfig>('dbConfig');
  const pool = new Pool(sqlConfig);

  // BIND SANITIZERS
  container.bind<Sanitizer<any>>(SanitizerSymbol)
    .toConstantValue(({password, ...user}) => user as any)
    .whenTargetNamed('password');

  container.bind<Sanitizer<any>>(SanitizerSymbol)
    .toConstantValue(({id, ...user}) => user as any)
    .whenTargetNamed('id');

  return container;
}