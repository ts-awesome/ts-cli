import { Container, interfaces } from 'inversify';
import { Request } from 'express';
import {
  IUnitOfWork,
  UnitOfWork,
  IQueryExecutorProvider,
  IEntityService,
  EntityService,
  Symbols as EntitySymbols
} from '@viatsyshyn/ts-entity';
import {IDbDataReader, IBuildableQueryCompiler, Symbols as OrmSymbols, IQueryDriver} from '@viatsyshyn/ts-orm';
import {ISqlQuery} from '@viatsyshyn/ts-orm-pg'

function bindServices(container: Container, ...entityClasses: Array<InstanceType<any>>): void {

  // bind entityServices
  entityClasses.forEach(Model => {
    const executorProvider = container.get<IQueryExecutorProvider<ISqlQuery>>(EntitySymbols.UnitOfWork);
    const queryBuilder = container.get<IBuildableQueryCompiler<ISqlQuery>>(OrmSymbols.SqlQueryBuilder);
    const dbDataRead = container.get<IDbDataReader<any>>(OrmSymbols.dbReaderFor(Model));
    container
      .bind<IEntityService<any, any, any>>(EntitySymbols.serviceFor(Model))
      .toConstantValue(new EntityService<any, ISqlQuery, any, any>(Model, executorProvider, queryBuilder, dbDataRead));
  });
}

export function setUpRequestContainer(container: Container, req: Request) {

  container
    .bind<IUnitOfWork<ISqlQuery>>(EntitySymbols.UnitOfWork)
    .toDynamicValue(({container}: interfaces.Context) => {
      const driver = container.get<IQueryDriver<ISqlQuery>>(OrmSymbols.SqlQueryDriver);
      return new UnitOfWork<ISqlQuery>(driver);
    })
    .inSingletonScope();

  bindServices(container)
}