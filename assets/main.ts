import 'reflect-metadata';
import 'source-map-support/register';

import './middleware';
import './routes';

import { setUpRootContainer } from './ioc-container';
import { ApplicationServer } from './app-server';

const container = setUpRootContainer();
const SERVER = new ApplicationServer(container);

if (process.env.NODE_ENV !== 'production') {
  SERVER.start();
}

export default SERVER;
