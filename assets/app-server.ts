import 'reflect-metadata';

import express, { Application, Response, Request, NextFunction } from 'express';
import * as bodyParser from 'body-parser';
import cors from 'cors';
import { Server } from "http";
import { Container } from 'inversify';
import { IConfig } from 'config';
import passport from 'passport';

import { ConfigSymbol } from "@viatsyshyn/ts-logger";
import { openApi, OpenApiHttpSecurityScheme } from '@viatsyshyn/ts-openapi';
import expressServerBuilder, { IHttpRequest, IHttpResponse, IErrorMiddleware } from '@viatsyshyn/ts-rest';

import { setUpRequestContainer } from './ioc-container';
import { ErrorHandlerMiddlewareSymbol } from "./middleware";

export class ApplicationServer {

  private expApp: Application;
  private server?: Server;
  private readonly port: number = 3000;

  constructor(container: Container) {

    this.expApp = this.createExpressApplication(container);
    const config = container.get<IConfig>(ConfigSymbol);

    if (config.has('port')) {
      this.port = config.get('port');
    }
  }

  public async start(): Promise<void> {
    this.server = this.expApp.listen(this.port, '0.0.0.0', () => {
      console.info(`Server is listening 0.0.0.0:${this.port}`);
    });
  }

  public stop(): void {
    this.server?.close();
  }

  private createExpressApplication(container: Container): Application {
    const app = express();

    app.use(cors());
    app.use(bodyParser.json());

    app.use(
      openApi({
        def: {
          info: {
            title: "TorrentTube api",
            version: "1.0"
          },
          components: {
            securitySchemes: {
              BearerAuth: {
                type: 'http',
                scheme: OpenApiHttpSecurityScheme.bearer,
                bearerFormat: 'JWT',
              }
            }
          },
        }
      })
    );

    // TODO move this to pre middleware maybe
    app.use(async (req: Request, res: Response, next: NextFunction) => {
      if (req.path.startsWith('/api-docs')) {
        return next();
      }

      await new Promise((resolve, reject) => {
        try {
          passport.authenticate('jwt', {session: false, failWithError: true})(req, res, (err: any) => resolve());
        } catch (e) {
          reject(e);
        }
      });

      next();
    });

    expressServerBuilder(app, container, setUpRequestContainer);

    app.use(((err: Error, req: IHttpRequest, res: IHttpResponse, next: NextFunction) => {
      container.get<IErrorMiddleware>(ErrorHandlerMiddlewareSymbol)
        .handle(err, req, res)
        .then(next);
    }) as any);

    return app;
  }
}