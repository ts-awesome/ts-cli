import { http{{method}}, {{imports.paramImports}} Route } from '@viatsyshyn/ts-rest';
import { OpenApiOperation{{method}} } from '@viatsyshyn/ts-openapi';

@http{{method}}('{{path}}')
export class {{routeName}} extends Route {

  @OpenApiOperation{{method}}({{openApi}})
  public async handle({{params}}): Promise<void> {
    return this.json('Hello from {{routeName}}!');
  }
}