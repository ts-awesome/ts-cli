import { dbTable, dbField } from "@viatsyshyn/ts-orm";

@dbTable('<%-name%>')
export class <%= name %>Model {
  <% attributes.forEach((attribute) => { %>
  @dbField(<% if (attribute.meta) { %>{
      <% for (const [key, value] of Object.entries(attribute.meta)) { %>
        ${key}: <%= typeof value === 'boolean' ? `${value}` : `'${value}'`%>,
        <%}%>}<%} %>)
  public <%-attribute.name%>!: <%-attribute.type%>;
  <% }); %>
};