import { injectable, inject } from 'inversify';

import {
  UnauthorizedError,
  RequestError,
  DuplicateFieldError,
  StatusCode
} from '../errors';

import { IErrorResult } from '../models/error-result.model';
import { IErrorMiddleware, IHttpRequest, IHttpResponse } from '@viatsyshyn/ts-rest';
import {ILoggerFactory, LoggerFactorySymbol} from '@viatsyshyn/ts-logger';

export const ErrorHandlerMiddlewareSymbol = Symbol.for('ErrorHandlerMiddleware');

@injectable()
export class ErrorHandlerMiddleware implements IErrorMiddleware {

  constructor(
    @inject(LoggerFactorySymbol) private readonly loggerFactory: ILoggerFactory
  ) { }

  public async handle(err: Error, req: IHttpRequest, res: IHttpResponse): Promise<void> {

    let errorResult: IErrorResult<unknown>;
    if (err instanceof UnauthorizedError) {
      errorResult = {
        error: `Not authorized`,
        code: err.statusCode,
        name: err.name,
      }
    } else if (err instanceof DuplicateFieldError) {
      errorResult = {
        error: err.message,
        code: err.statusCode,
        name: err.name,
        data: {
          field: err.field,
          value: err.value
        }
      }
    }
    else if (err instanceof RequestError && err.statusCode !== StatusCode.ServerError) {
      errorResult = {
        error: err.message,
        code: err.statusCode,
        name: err.name,
      }
    } else {
      const loggerName = req.url;
      const logger = this.loggerFactory(loggerName);
      logger.error(err, req, req.user);

      errorResult = {
        error: err.message,
        code: StatusCode.ServerError,
        name: err.name,
      }
    }

    return res
      .status(errorResult.code)
      .json(errorResult)
      .end();
  }
}
